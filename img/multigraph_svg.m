%funcion que dibuja la curva granulometrica RGB  
%Se asume que v es un array y p es un entero que indica la cantidad de valores por cada elemento 
% de esta manera si existen N elementos de serie y P valores para cada elemento el tamaño del array debe ser N*P   
function res  = multigraph_svg ( v, p , output , titulo, labels )
    np = length ( v ) ;
    res = zeros(p, np/p );
    disp("np =") , disp(np);
    disp("np/p =") ,  disp(np/p);

    i = 1 ;
    cult = 1 ;
    while(i <= np )
	for c = cult:( cult  + (p - 1) )            
	for f = 1:p
               if i > np
                  break;  
               endif 
               res(f,c) = v( i ) ;
               i = i + 1 ;


 

        endfor  
        endfor
        cult = cult + p ;

    endwhile

YR= res(1,:);
YG= res(2,:);
YB= res(3,:);
XR= 1:np/p;
XG= XR;
XB = XR ;



grosorlinea = 2 ; 

set(gca, "linewidth", 3, "fontsize", 12)


plot( XR , YR ,  ":+r" ,"linewidth",grosorlinea,  XG, YG, ":og" , "linewidth",grosorlinea , XB, YB, ":.b", "linewidth",grosorlinea);
hx = xlabel (labels(1)); 
set (hx, "fontsize", 14) ;

hy = ylabel(labels(2));
set (hy, "fontsize", 14) ;
#set (hy,'Position',[0 100000 0]); 
#set (hy,'Position',[0 9000 0]); 


htitle = title( titulo );
set (htitle, "fontsize", 15) 

#legend('  ', 'FontName', 'Arial');

set([gca; findall(gca, 'Type','text')], 'FontName', 'Arial');

%print -color -dpdfwrite output
print(output, '-color', '-deps');
endfunction


