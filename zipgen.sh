#!/bin/bash

# select files and  generate zip to upload 

find . -type f -name "Fig*" \
	-o -name "Ref*" \
	-o -name "*report.tex" \
	-o -name "svglov3*" \
	-o -name "svjour3*" \
	-o -name "spbasic.bst" \
	-o -name "title_page.txt" | zip nelsonds.toupload.zip -@

